// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CS378_LAB4_CS378_Lab4GameModeBase_generated_h
#error "CS378_Lab4GameModeBase.generated.h already included, missing '#pragma once' in CS378_Lab4GameModeBase.h"
#endif
#define CS378_LAB4_CS378_Lab4GameModeBase_generated_h

#define CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_SPARSE_DATA
#define CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_RPC_WRAPPERS
#define CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACS378_Lab4GameModeBase(); \
	friend struct Z_Construct_UClass_ACS378_Lab4GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ACS378_Lab4GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab4"), NO_API) \
	DECLARE_SERIALIZER(ACS378_Lab4GameModeBase)


#define CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesACS378_Lab4GameModeBase(); \
	friend struct Z_Construct_UClass_ACS378_Lab4GameModeBase_Statics; \
public: \
	DECLARE_CLASS(ACS378_Lab4GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Lab4"), NO_API) \
	DECLARE_SERIALIZER(ACS378_Lab4GameModeBase)


#define CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACS378_Lab4GameModeBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACS378_Lab4GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACS378_Lab4GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACS378_Lab4GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACS378_Lab4GameModeBase(ACS378_Lab4GameModeBase&&); \
	NO_API ACS378_Lab4GameModeBase(const ACS378_Lab4GameModeBase&); \
public:


#define CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACS378_Lab4GameModeBase(ACS378_Lab4GameModeBase&&); \
	NO_API ACS378_Lab4GameModeBase(const ACS378_Lab4GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACS378_Lab4GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACS378_Lab4GameModeBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACS378_Lab4GameModeBase)


#define CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_12_PROLOG
#define CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_SPARSE_DATA \
	CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_RPC_WRAPPERS \
	CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_INCLASS \
	CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_SPARSE_DATA \
	CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CS378_LAB4_API UClass* StaticClass<class ACS378_Lab4GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID CS378_Lab4_Source_CS378_Lab4_CS378_Lab4GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

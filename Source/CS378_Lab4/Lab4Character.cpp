 // Fill out your copyright notice in the Description page of Project Settings.


#include "Lab4Character.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Sets default values
ALab4Character::ALab4Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	InteractionLength = 3.0f;

	maxWalkSpeed = 100.f;
	maxRunSpeed = 600.f;

	GetCharacterMovement()->MaxWalkSpeed = maxRunSpeed;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->TargetArmLength = 400.f;
	CameraBoom->SetRelativeRotation(FRotator(0.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("OvertheShoulderCamera"));
	CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false;

}
 
// Called when the game starts or when spawned
void ALab4Character::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALab4Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ALab4Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

bool ALab4Character::CanPerformAction(ECharacterActionStateEnum updatedAction)
{
	switch (CharacterActionState)
	{
		case ECharacterActionStateEnum::IDLE:
			return true;
			break;
		case ECharacterActionStateEnum::MOVE:
			if(updatedAction != ECharacterActionStateEnum::INTERACT){
				return true;
			}
			break;
		case ECharacterActionStateEnum::JUMP:
			if(updatedAction == ECharacterActionStateEnum::IDLE || updatedAction == ECharacterActionStateEnum::MOVE) {
				return true;
			}
			break;
		case ECharacterActionStateEnum::INTERACT:
			return false;
			break;
	}
	return false;
} 

void ALab4Character::ApplyMovement(float scale)
{
	AddMovementInput(GetActorForwardVector(), scale);
}

void ALab4Character::ApplyStrafe(float scale)
{
	AddMovementInput(GetActorRightVector(), scale);
}

void ALab4Character::BeginInteraction()
{
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Interaction Started"));
	GetWorld()->GetTimerManager().SetTimer(InteractionTimerHandle, this, &ALab4Character::EndInteraction, InteractionLength);
}

void ALab4Character::EndInteraction()
{
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("Interaction Ended"));
	UpdateActionState(ECharacterActionStateEnum::IDLE);
}

void ALab4Character::UpdateActionState(ECharacterActionStateEnum newAction)
{
	if (newAction == ECharacterActionStateEnum::MOVE || newAction == ECharacterActionStateEnum::IDLE){
		if (FMath::Abs(GetVelocity().Size()) <= 0.01f) {
			CharacterActionState = ECharacterActionStateEnum::IDLE;
		} else {
			CharacterActionState = ECharacterActionStateEnum::MOVE;
		}
	} else {
		CharacterActionState = newAction;
	}
}

void ALab4Character::ToggleWalkSpeed() {
	if (GetCharacterMovement()->MaxWalkSpeed == maxWalkSpeed) {
		GetCharacterMovement()->MaxWalkSpeed = maxRunSpeed;
	} else {
		GetCharacterMovement()->MaxWalkSpeed = maxWalkSpeed;
	}
}